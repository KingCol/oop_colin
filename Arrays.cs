﻿using UnityEngine;
using System.Collections;

public class Arrays : MonoBehaviour 
{
	public GameObject[] players;
	// Use this for initialization
	void Start () 
	{
	// collects information on how many gameobjects with tag "Player" within the scene at runtime
	// using a for loop, this will iterate through every "Player" using players.Length
	// for each player a message is displayed with the currentl iterated player's name
		players = GameObject.FindGameObjectsWithTag("Player");
		
		for(int i = 0; i < players.Length; i++)
		{
			Debug.Log("Player Number "+i+" is named "+players[i].name);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
