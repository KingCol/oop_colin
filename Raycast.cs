﻿using UnityEngine;
using System.Collections;

public class Raycast : MonoBehaviour 
{
	public float rangeDetect = 10f;
	public float moveSpeed = 2.5f;
	// Use this for initialization
	void Start ()
	{
	
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
		
	// takes in the position the ray, takes in the direction of the ray, takes in the hit information of the array, and takes in the range of the ray
	// creating a variable using the Ray datatype allows you to store both the position and direction of the array in one parameter
	// out hit records the information of which collider the ray collides with, which allows the use of doing something with it
	
		RaycastHit hit;
		Ray ray = new Ray(transform.position, Vector3.forward);
		Debug.DrawRay(transform.position, Vector3.forward * rangeDetect, Color.red);
		
		if(Physics.Raycast (ray, out hit, rangeDetect))
		{
			if(hit.collider.tag == "Wall")
			{
				StopMovement();
			}
		}
	}
	
	void StopMovement()
	{
		moveSpeed = 0;	
	}
}
