﻿using UnityEngine;
using System.Collections;

public class Loops : MonoBehaviour 
{
	public int countDown = 5;
	public int objectCount = 5;
	// Use this for initialization
	void Start () 
	{
		WhileLoop ();
		DoWhileLoop ();
		ForLoop ();
		ForEachLoop ();
	}
	
	void WhileLoop()
	{
		while(countDown > 0)
		{
			Debug.Log("Current number is" + " " + countDown);
			countDown--;
		}
	}
	
	void DoWhileLoop()
	{
		bool sendTextOnce = false;
		
		do
		{
			print ("Text has been sent.");
		}
		
		while(sendTextOnce == true);
	}
	
	void ForLoop()
	{
	// if the index number specified is lower than the number it is compared with, the index number will keep iterating until it meets that number's quota
		for(int i = 0; i < objectCount; i++)
		{
			Debug.Log("Count:" + i);
		}
	}
	
	void ForEachLoop()
	{
	// creates an array that hold multiple strings which then iterates through each element within that array 
		string[] strings = new string[5];
		
		strings[0] = "Text One";
		strings[1] = "Text Two";
		strings[2] = "Text Three";
		strings[3] = "Text Three";
		strings[4] = "Text Four";
		
		foreach(string item in strings)
		{
			print (item);
		}
	}
}
